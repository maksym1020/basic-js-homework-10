//  1. Створіть новий елемент <a>, задайте йому текст "Learn More" і атрибут href
//  з посиланням на "#". Додайте цей елемент в footer після параграфу.

const footer = document.body.querySelector('footer');

const footerLink = document.createElement('a');
footerLink.setAttribute('href', '#');
footerLink.innerText = 'Learn More';
footer.append(footerLink);

// or

// const footerLink2 = footerLink.cloneNode(true);
// footerLink2.href = '##';
// footer.append(footerLink2);


//
//  2. Створіть новий елемент <select>. Задайте йому ідентифікатор "rating", і додайте його в тег main перед секцією "Features".
//  Створіть новий елемент <option> зі значенням "4" і текстом "4 Stars", і додайте його до списку вибору рейтингу.
//  Створіть новий елемент <option> зі значенням "3" і текстом "3 Stars", і додайте його до списку вибору рейтингу.
//  Створіть новий елемент <option> зі значенням "2" і текстом "2 Stars", і додайте його до списку вибору рейтингу.
//  Створіть новий елемент <option> зі значенням "1" і текстом "1 Star", і додайте його до списку вибору рейтингу.
//

const features = document.querySelector('.features');

const ratingSelect = document.createElement('select');
ratingSelect.id = 'rating';
features.before(ratingSelect);

const fragment = document.createDocumentFragment();

function createOptions() {
    for (let i = 4; i > 0; i--) {
        let option = document.createElement('option');
        option.value = `${i}`;
        option.textContent = `${i} Starts`;
        fragment.append(option);
    }
}

createOptions();

ratingSelect.append(fragment);





